# RSSReader

The goal of this assignment is to retrieve news in the RSS format from a remote server and display the contents to a user. It must be solved in
ASP.NET using C# and JavaScript. The choice between ASP.NET WebForms or ASP.NET MVC is entirely up to you.
Implement a view of a RSS news feed, where at least these following fields can be read:
Title
Publication date
Link to each news item in the feed, so that clicking the link will take the end-user to that external news page
Make the RSS URL editable, so that the end-user is able to change it
Provide a sort option for the end-user

## How you handled the assignments in general?
I carrefully read each one of the assignments and tryied to figure out how I'll solve both of them in my head. 

## How much time you have put into each sub-task and how you prioritized the different assignments and sub-tasks?
I realized that RSS Reader assignment will take more time, cause it's more laborious but not so dificult and perhaps may show more of my skills.
I tried to divide the features in branchs for the first one and do them in order. 

## Why you have constructed the code as you have?
I created a separated project to hold the models cause we may use it in other projects. 
I used SyndicationFeed to be more simple and straightforward. I also used a strongly type view to hold the list to cause I think thats better to developer upon it.
The JQuery's use was just to be more simple. I also desired to create classes for that(ECMAScript 6+) or use Angular, but as it was a simple task I just adopted simple Jquery.

## What have surprised you the most during the coding challenge?
No surprises.
