﻿namespace Models
{
    /// <summary>
    /// Class to model an RSS Reader item
    /// </summary>
    public class RssReaderSimpleItem
    {
        public string Title { get; private set; }
        public string Uri { get; private set; }

        public RssReaderSimpleItem(string title, string uri)
        {
            Title = title;
            Uri = uri;
        }
    }
}
