﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Syndication;
using Microsoft.AspNetCore.Mvc;
using System.Xml;
using Models;
using RSSReader.Models;

namespace RSSReader.Controllers
{
    public class HomeController : Controller
    {
        private const string RSS_LINK = "http://feeds.jn.pt/JN-Ultimas";

        public IActionResult Index()
        {
            return View(GetRssItems());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        /// <summary>
        /// Must get a list of items from a RSS Feed
        /// </summary>
        /// <returns></returns>
        private List<RssReaderSimpleItem> GetRssItems()
        {
            List<RssReaderSimpleItem> itemsList = new List<RssReaderSimpleItem>();
            
            using (var reader = XmlReader.Create(RSS_LINK))
            {
                SyndicationFeed feed = SyndicationFeed.Load(reader);

                foreach (var item in feed.Items)
                {
                    itemsList.Add(new RssReaderSimpleItem(title:item.Title.Text,
                                                          uri:  item.Links.FirstOrDefault().Uri.ToString()));
                }
            }
            return itemsList;
        }
    }
}
