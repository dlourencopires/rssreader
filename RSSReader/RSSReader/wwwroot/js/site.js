﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(document).ready(function () {
    var isEditing = false;

    $('span').click(function (a) {
        if (isEditing)
        {
            alert("You need to finished the previous edition to edit another one!");
            return;
        };

        isEditing = true;
        var span = a.currentTarget;
        var anchor = span.previousElementSibling;
        var url = anchor.attributes[0].value;

        var input = $("<input title='Press enter to finish' type='text' value='" + url + "'/>").insertAfter(span);
        input.on('keypress', function (e) {
            if (e.which == 13) {
                anchor.attributes[0].value = input.val();
                input.remove();
                isEditing = false;
            }
        });
    });

    $('#sortingAZ').click(function () {
        ImplementSorting(-1, 1);
    });

    $('#sortingZA').click(function () {
        ImplementSorting(1, -1);
    });

    function ImplementSorting(firstOrderInt, secondOrderInt) {
        var ul = $('ul.list');
        var items = $('ul.list > li').get();

        items.sort(function (first, second) {
            var firstText = $(first).text();
            var secondText = $(second).text();

            if (firstText < secondText)
                return firstOrderInt;

            if (firstText > secondText)
                return secondOrderInt;

            return 0;
        });

        $.each(items, function (i, li) {
            ul.append(li);
        });
    }
});
